<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::middleware(['auth:sanctum', 'ensure_user'])->group(function () {
    Route::post('companies/{company}', [CompanyController::class, 'update']);
    Route::get('companies-list', [CompanyController::class, 'companyList']);
    Route::get('dashboard-count', [CompanyController::class, 'dashbordCount']);
    Route::resource('companies', CompanyController::class);
    Route::post('employees/{employee}', [EmployeeController::class, 'update']);
    Route::resource('employees', EmployeeController::class);
});
