<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;

class UserEnsure
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (Auth::check() && Auth::user()->email !== 'admin@grtech.com') {
            Auth::logout();
            if ($request->header('X-Inertia')) {
                return Inertia::location(route('login'));
            }
            return redirect()->route('login')->withErrors(['message' => 'Unauthorizes user.']);
        }

        return $next($request);
    }
}
