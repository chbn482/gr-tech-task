<?php

namespace App\Http\Controllers;
use App\Models\Company;
use App\Models\Employee;
use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use Illuminate\Support\Facades\Storage;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $companies = Company::orderBy('created_at' , 'desc')->paginate(10);
        return response()->json($companies);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CompanyRequest $request)
    {
        $data = $request->validated();
        if ($request->hasFile('logo')) {
            $path = $request->file('logo')->store('logos', 'public');
            $data['logo'] = $path;
        }
        $company = Company::create($data);
        $message= "Operation completed successfully.";
        return response()->json(['company'=>$company,'message'=>$message], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(Company $company)
    {
        return response()->json($company);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CompanyRequest $request, Company $company)
    {
       $data = $request->validated();
        if ($request->hasFile('logo')) {
            if ($company->logo) Storage::delete($company->logo);
            $data['logo'] = $request->file('logo')->store('logos', 'public');
        }elseif (filter_var($data['logo'], FILTER_VALIDATE_URL)) {
            unset($data['logo']);
        }
        $company->update($data);
        $message= "Operation completed successfully.";
        return response()->json(['company'=>$company,'message'=>$message], 201);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Company $company)
    {
        $company->delete();
        return response()->json(['message'=>'Operation completed successfully.'], 201);
    }
    /**
     * Display a listing of the for employees.
     */
    public function companyList()
    {
        $companies = Company::all();
        return response()->json($companies);
    }

    /**
     * Display a count of the for employees and comp.
     */
    public function dashbordCount()
    {
        $data['companies'] = Company::count();
        $data['employees'] = Employee::count();
        return response()->json($data);
    }
}
