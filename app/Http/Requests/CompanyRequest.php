<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $uniqueRule = 'unique:companies,email';
        if ($this->company) {
            $uniqueRule .= ',' . $this->company->id;
        }
        return [
            'name' => 'required|string|max:255',
            'email' => ['required', 'email', $uniqueRule],
            'website' => 'nullable|url',
            'logo' => (function() {
                $rules = [];
                if (filter_var($this->logo, FILTER_VALIDATE_URL)) {
                    $rules = [];
                }
                else if (($this->method() === 'POST' || $this->hasFile('logo'))) {
                    $rules = ['nullable','image','max:5120'];
                }
                return $rules;
            })(),
        ];
    }
}
