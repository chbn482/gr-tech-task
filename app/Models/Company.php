<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Casts\StorageUrlCast;

class Company extends Model
{
    use HasFactory, Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'logo', 'website'];
    
    /**
     * Get employee associated with model.
     */
    public function employees()
    {
        return $this->hasMany(Employee::class);
    }
    protected $casts =[
        'logo'=> StorageUrlCast::class,
    ];
}
