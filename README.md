# Laravel GR TECH TASK Project Setup Guide
https://drive.google.com/file/d/1UOZLau66RBAVwXipjGMDy_MBeI27RLzo/view?usp=sharing
Video Link
## Introduction

This guide outlines the steps necessary to set up the Laravel project on your local development environment. Follow these instructions to clone the project, install dependencies, set up the database, and run the project.

## Prerequisites

Ensure you have the following installed on your system:
- Git
- PHP (8.1 or higher recommended)
- Composer
- Node.js (with npm)

## Installation

### Step 1: Clone the Repository

Clone the project repository to your local machine using the following command:


git clone https://bitbucket.org/chbilal/gr-tech-task.git
OR
git clone https://gitlab.com/chbn482/gr-tech-task
cd gr-tech-task

## Step 2: Install Dependencies
Install Composer dependencies:
- composer install

- Install npm packages:

npm install

## Step 3: Set Up the Environment File
Copy the .env.example file to a new file named .env

## Step 4: Database Configuration
Create a new database on your local MySQL server.
Open the .env file and update the database settings (DB_DATABASE, DB_USERNAME, and DB_PASSWORD) to match your local environment.

## Step 5: Database Migration and Seeding
Run the migrations and seed the database:

- php artisan migrate
- php artisan db:seed --class=UsersTableSeeder

## Step 6: Generate Application Key
Generate a new unique application key with the following Artisan command:

- php artisan key:generate

## Step 7: Compile Assets
Compile the frontend assets using npm:
- npm run dev

## Step 8: Start the Application
Start the Laravel development server:

- php artisan serve

Here the project is ready to run!!


## Accessing the Application
With the server running, you can access the application in your web browser at http://127.0.0.1:8000/login.

## Default User Credentials

- Administrator Account
Email: admin@grtech.com
Password: password

- User Account
Email: user@grtech.com
Password: password



