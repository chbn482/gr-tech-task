import axios from 'axios';
const axiosInstance = axios.create({
    baseURL: '/api',
});

// Token Get
function getAuthToken() {
    return localStorage.getItem('authToken'); 
}

// Interceptor to add the token to each request
axiosInstance.interceptors.request.use((config) => {
    const token = getAuthToken();
    if (token) {
        config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
}, (error) => {
    return Promise.reject(error);
});

export default axiosInstance;
